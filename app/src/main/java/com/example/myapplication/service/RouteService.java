package com.example.myapplication.service;

import com.example.myapplication.model.marker.Marker;
import com.example.myapplication.repository.IMarkerDao;
import com.example.myapplication.repository.IRouteDao;
import com.example.myapplication.repository.RouteDao;
import com.example.myapplication.model.route.Route;

import java.util.List;

import io.realm.RealmList;

public class RouteService implements IRouteService {
    private IRouteDao  routeDao;
    private IMarkerDao markerDao;

    public RouteService() {
        routeDao = new RouteDao();
    }
    public RouteService(IRouteDao routeDao, IMarkerDao markerDao) {
        this.routeDao  = routeDao;
        this.markerDao = markerDao;
    }
    @Override
    public void addRoute(Route route) {
        routeDao.addRoute(route);
    }

    @Override
    public void removeRoute(int id) {
        routeDao.removeRoute(id);
    }

    @Override
    public List<Route> getAllRoutes() {
        return routeDao.getAllRoutes();
    }

    @Override
    public Route getRoute(int id) {
        return routeDao.getRoute(id);
    }

    @Override
    public void deleteAll() {
        routeDao.deleteAll();
    }

    @Override
    public List<Route> sortByName() {
        return routeDao.sortByName();
    }

    @Override
    public List<Route> sortById() {
        return routeDao.sortById();
    }

    @Override
    public List<Route> sortByCountMarkers() {
        return routeDao.sortByCountMarkers();
    }



    @Override
    public List<Marker> getAllMarkers(int id) {
        List<Marker> markers = new RealmList<Marker>();

        Route route = getRoute(id);
        for(Integer markerId: route.getMarkersId()) {
            markers.add(markerDao.getMarker(markerId));
        }
        return markers;
    }
}
