package com.example.myapplication.service;

import com.example.myapplication.model.authorization.User;
import com.example.myapplication.repository.IUserDao;

public class UserService implements IUserService {
    private IUserDao userDao;

    public  UserService(IUserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    public void addUser(User user) {
        userDao.addUser(user);
    }

    @Override
    public void removeUser() {
        userDao.removeUser();
    }

    @Override
    public User getUser() {
        return userDao.getUser();
    }

    @Override
    public boolean haveUser() {
        return userDao.haveUser();
    }
}
