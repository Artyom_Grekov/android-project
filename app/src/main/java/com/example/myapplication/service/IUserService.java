package com.example.myapplication.service;

import com.example.myapplication.model.authorization.User;

public interface IUserService {
    void addUser(User user);
    void removeUser();
    User getUser();
    boolean haveUser();
}
