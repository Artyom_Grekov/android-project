package com.example.myapplication.service;

import com.example.myapplication.model.marker.Marker;
import com.example.myapplication.repository.IMarkerDao;
import com.example.myapplication.repository.MarkerDao;

import java.util.List;

public class MarkerService implements IMarkerService {
    private IMarkerDao markerDao;

    public MarkerService() {
        this.markerDao = new MarkerDao();
    }
    public MarkerService(IMarkerDao markerDao) {
        this.markerDao = markerDao;
    }
    @Override
    public void addMarker(Marker marker) {
        this.markerDao.addMarker(marker);
    }

    @Override
    public void removeMarker(int id) {
        this.markerDao.removeMarker(id);
    }

    @Override
    public List<Marker> getAllMarker() {
        return this.markerDao.getAllMarker();
    }

    @Override
    public Marker getMarker(int id) {
        return this.markerDao.getMarker(id);
    }
}
