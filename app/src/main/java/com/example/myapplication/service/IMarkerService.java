package com.example.myapplication.service;

import com.example.myapplication.model.marker.Marker;

import java.util.List;

public interface IMarkerService {
    void addMarker(Marker marker);
    void removeMarker(int id);
    List<Marker> getAllMarker();
    Marker getMarker(int id);
}
