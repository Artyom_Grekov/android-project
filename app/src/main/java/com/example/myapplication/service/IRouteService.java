package com.example.myapplication.service;

import com.example.myapplication.model.marker.Marker;
import com.example.myapplication.model.route.Route;

import java.util.List;

public interface IRouteService {
    void addRoute(Route route);
    void removeRoute(int id);
    List<Route> getAllRoutes();
    Route getRoute(int id);
    void deleteAll();
    List<Route> sortByName();
    List<Route> sortById();
    List<Route> sortByCountMarkers();
    List<Marker> getAllMarkers(int id);
}
