package com.example.myapplication.service;

import android.content.Context;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.myapplication.R;
import com.example.myapplication.Singleton.Singleton;
import com.example.myapplication.Thread.LoadMarkersThread;
import com.example.myapplication.model.marker.LightweightMarker;
import com.example.myapplication.model.marker.LightweightMarkers;
import com.example.myapplication.model.marker.Marker;
import com.example.myapplication.retrofit.NetworkService;
import com.example.myapplication.model.route.Route;
import com.example.myapplication.model.route.Routes;
import com.example.myapplication.model.authorization.RequestAuthorization;
import com.example.myapplication.model.authorization.User;
import com.example.myapplication.retrofit.interfaces.YouGidApi;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WebService {
    private IRouteService  routeService;
    private IMarkerService markerService;

    public WebService() {
        this.routeService  = Singleton.getRouteService();
        this.markerService = Singleton.getMarkerService();
    }

    public void loadRoutes(final Context context) {
        String token = Singleton.getUserService().getUser().getToken();

        NetworkService.getInstance().getYouGidApi().getAllRoutes("Token " + token, "517").enqueue(new Callback<Routes>() {
            @Override
            public void onResponse(Call<Routes> call, Response<Routes> response) {
                for(Route route: response.body().getResults()) {
                    //routeService.addRoute(route);

                    loadLightMarkersList(route);
                }

                String text = "Успешно";
                int duration = Toast.LENGTH_SHORT;

                Toast toast = Toast.makeText(context, text, duration);
                //toast.show();
            }

            @Override
            public void onFailure(Call<Routes> call, Throwable t) {
                String text = "Ошибка";
                int duration = Toast.LENGTH_SHORT;

                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
            }
        });
    }

    public void authorization(String name, String password, final AppCompatActivity context) {
        RequestAuthorization request = new RequestAuthorization();
        request.setUsername(name);
        request.setPassword(password);

        NetworkService.getInstance().getYouGidApi().authorization(request).enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                User responseA = response.body();

                if(responseA == null) {
                    String text = "Неверные данные";
                    int duration = Toast.LENGTH_SHORT;

                    Toast toast = Toast.makeText(context, text, duration);
                    toast.show();
                    return;
                }

                NetworkService.getInstance().setToken(responseA.getToken());
                Singleton.getUserService().addUser(responseA);

                context.finish();
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                String text = "Ошибка с сетью";
                int duration = Toast.LENGTH_SHORT;

                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
            }
        });
    }


    public void loadLightMarkersList(final Route route) {
        int routeId = route.getId();

        NetworkService.getInstance().getYouGidApi().getLightweightMarkersList(routeId).enqueue(new Callback<List<LightweightMarker>>() {
            @Override
            public void onResponse(Call<List<LightweightMarker>> call, Response<List<LightweightMarker>> response) {
                List<LightweightMarker> marker = response.body();

                loadMarkers(marker, route);
            }

            @Override
            public void onFailure(Call<List<LightweightMarker>> call, Throwable t) {
                int k = 5 + 4;

            }
        });
    }

    public void loadMarkers(List<LightweightMarker> lightMarkers,final Route route) {

        new LoadMarkersThread(route, lightMarkers).start();

        /*for(LightweightMarker lightMarker: lightMarkers) {
            try {
                Response<Marker> response = NetworkService.getInstance().getYouGidApi().getMarker(lightMarker.getId()).execute();

                Marker marker = response.body();
                markerService.addMarker(marker);

                route.addMarker(marker);
            } catch (IOException e) {
                e.printStackTrace();
            }

            routeService.addRoute(route);*/

            /*NetworkService.getInstance().getYouGidApi().getMarker(lightMarker.getId()).enqueue(new Callback<Marker>() {
                @Override
                public void onResponse(Call<Marker> call, Response<Marker> response) {
                    Marker marker = response.body();
                    markerService.addMarker(marker);

                    route.addMarker(marker);
                }

                @Override
                public void onFailure(Call<Marker> call, Throwable t) {

                }
            });*/
        //}
    }

    public void loadImage(ImageView view, String url) {
        Picasso.get().load(url).placeholder(R.drawable.man).error(R.drawable.cross).into(view);
    }
}
