package com.example.myapplication.model.route;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

public class RoutesInstructor extends RealmObject {

    @SerializedName("routes_instructor_post")
    @Expose
    private String routesInstructorPost;
    @SerializedName("routes_instructor_photo")
    @Expose
    private String routesInstructorPhoto;
    @SerializedName("routes_instructor_name")
    @Expose
    private String routesInstructorName;
    @SerializedName("routes_instructor_reference")
    @Expose
    private String routesInstructorReference;
    @SerializedName("routes_instructor_type")
    @Expose
    private String routesInstructorType;
    @SerializedName("routes_instructor_experience")
    @Expose
    private String routesInstructorExperience;

    public String getRoutesInstructorPost() {
        return routesInstructorPost;
    }

    public void setRoutesInstructorPost(String routesInstructorPost) {
        this.routesInstructorPost = routesInstructorPost;
    }

    public String getRoutesInstructorPhoto() {
        return routesInstructorPhoto;
    }

    public void setRoutesInstructorPhoto(String routesInstructorPhoto) {
        this.routesInstructorPhoto = routesInstructorPhoto;
    }

    public String getRoutesInstructorName() {
        return routesInstructorName;
    }

    public void setRoutesInstructorName(String routesInstructorName) {
        this.routesInstructorName = routesInstructorName;
    }

    public String getRoutesInstructorReference() {
        return routesInstructorReference;
    }

    public void setRoutesInstructorReference(String routesInstructorReference) {
        this.routesInstructorReference = routesInstructorReference;
    }

    public String getRoutesInstructorType() {
        return routesInstructorType;
    }

    public void setRoutesInstructorType(String routesInstructorType) {
        this.routesInstructorType = routesInstructorType;
    }

    public String getRoutesInstructorExperience() {
        return routesInstructorExperience;
    }

    public void setRoutesInstructorExperience(String routesInstructorExperience) {
        this.routesInstructorExperience = routesInstructorExperience;
    }

}