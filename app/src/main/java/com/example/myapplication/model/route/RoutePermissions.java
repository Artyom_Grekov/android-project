package com.example.myapplication.model.route;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

public class RoutePermissions extends RealmObject {
    @SerializedName("add_route")
    @Expose
    private Boolean addRoute;
    @SerializedName("change_route")
    @Expose
    private Boolean changeRoute;
    @SerializedName("change_waypoints")
    @Expose
    private Boolean changeWaypoints;
    @SerializedName("delete_route")
    @Expose
    private Boolean deleteRoute;
    @SerializedName("delete_waypoints")
    @Expose
    private Boolean deleteWaypoints;
    @SerializedName("view_route")
    @Expose
    private Boolean viewRoute;
    @SerializedName("view_waypoints")
    @Expose
    private Boolean viewWaypoints;

    public Boolean getAddRoute() {
        return addRoute;
    }

    public void setAddRoute(Boolean addRoute) {
        this.addRoute = addRoute;
    }

    public Boolean getChangeRoute() {
        return changeRoute;
    }

    public void setChangeRoute(Boolean changeRoute) {
        this.changeRoute = changeRoute;
    }

    public Boolean getChangeWaypoints() {
        return changeWaypoints;
    }

    public void setChangeWaypoints(Boolean changeWaypoints) {
        this.changeWaypoints = changeWaypoints;
    }

    public Boolean getDeleteRoute() {
        return deleteRoute;
    }

    public void setDeleteRoute(Boolean deleteRoute) {
        this.deleteRoute = deleteRoute;
    }

    public Boolean getDeleteWaypoints() {
        return deleteWaypoints;
    }

    public void setDeleteWaypoints(Boolean deleteWaypoints) {
        this.deleteWaypoints = deleteWaypoints;
    }

    public Boolean getViewRoute() {
        return viewRoute;
    }

    public void setViewRoute(Boolean viewRoute) {
        this.viewRoute = viewRoute;
    }

    public Boolean getViewWaypoints() {
        return viewWaypoints;
    }

    public void setViewWaypoints(Boolean viewWaypoints) {
        this.viewWaypoints = viewWaypoints;
    }
}
