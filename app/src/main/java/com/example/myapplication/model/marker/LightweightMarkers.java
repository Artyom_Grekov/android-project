package com.example.myapplication.model.marker;

import java.util.ArrayList;
import java.util.List;

public class LightweightMarkers {
    public List<Marker> getResults() {
        return results;
    }

    public void setResults(List<Marker> results) {
        this.results = results;
    }

    private List<Marker> results = new ArrayList<Marker>();
}
