package com.example.myapplication.model.authorization;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

public class User extends RealmObject {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("user")
    @Expose
    private Integer user;
    @SerializedName("avatar")
    @Expose
    private String avatar;
    @SerializedName("full_name")
    @Expose
    private String fullName;
    @SerializedName("location")
    @Expose
    private UserLocation location;
    @SerializedName("token")
    @Expose
    private String token;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("sex")
    @Expose
    private Integer sex;
    @SerializedName("birthday")
    @Expose
    private String birthday;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("skype")
    @Expose
    private String skype;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("vk")
    @Expose
    private String vk;
    @SerializedName("facebook")
    @Expose
    private String facebook;


    // Getter Methods

    public float getId() {
        return id;
    }

    public float getUser() {
        return user;
    }

    public String getAvatar() {
        return avatar;
    }

    public String getFull_name() {
        return fullName;
    }

    public UserLocation getLocation() {
        return location;
    }

    public String getToken() {
        return token;
    }

    public String getLast_name() {
        return lastName;
    }

    public String getFirst_name() {
        return firstName;
    }

    public float getSex() {
        return sex;
    }

    public String getBirthday() {
        return birthday;
    }

    public String getPhone() {
        return phone;
    }

    public String getSkype() {
        return skype;
    }

    public String getEmail() {
        return email;
    }

    public String getVk() {
        return vk;
    }

    public String getFacebook() {
        return facebook;
    }

    // Setter Methods

    public void setId(Integer id) {
        this.id = id;
    }

    public void setUser(Integer user) {
        this.user = user;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public void setFull_name(String fullName) {
        this.fullName = fullName;
    }

    public void setLocation(UserLocation location) {
        this.location = location;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public void setLast_name(String lastName) {
        this.lastName = lastName;
    }

    public void setFirst_name(String firstName) {
        this.firstName = firstName;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setSkype(String skype) {
        this.skype = skype;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setVk(String vk) {
        this.vk = vk;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }
}
