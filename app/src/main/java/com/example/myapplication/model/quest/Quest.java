package com.example.myapplication.model.quest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmList;
import io.realm.annotations.Ignore;

public class Quest {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("permissions")
    @Expose
    @Ignore
    private Object permissions;
    @SerializedName("markers")
    @Expose
    private RealmList<Integer> markers = null;
    @SerializedName("markers_count")
    @Expose
    private Integer markersCount;
    @SerializedName("logo")
    @Expose
    private String logo;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("completed_tasks_count")
    @Expose
    private Integer completedTasksCount;
    @SerializedName("all_tasks_count")
    @Expose
    private Integer allTasksCount;
    @SerializedName("progress")
    @Expose
    private String progress;
    @SerializedName("completed")
    @Expose
    private Boolean completed;
    @SerializedName("payment")
    @Expose
    @Ignore
    private Object payment;
    @SerializedName("aborted")
    @Expose
    private Boolean aborted;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("deleted")
    @Expose
    private Boolean deleted;
    @SerializedName("date_created")
    @Expose
    private String dateCreated;
    @SerializedName("polymorphic_ctype")
    @Expose
    private Integer polymorphicCtype;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Object getPermissions() {
        return permissions;
    }

    public void setPermissions(Object permissions) {
        this.permissions = permissions;
    }

    public RealmList<Integer> getMarkers() {
        return markers;
    }

    public void setMarkers(RealmList<Integer> markers) {
        this.markers = markers;
    }

    public Integer getMarkersCount() {
        return markersCount;
    }

    public void setMarkersCount(Integer markersCount) {
        this.markersCount = markersCount;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getCompletedTasksCount() {
        return completedTasksCount;
    }

    public void setCompletedTasksCount(Integer completedTasksCount) {
        this.completedTasksCount = completedTasksCount;
    }

    public Integer getAllTasksCount() {
        return allTasksCount;
    }

    public void setAllTasksCount(Integer allTasksCount) {
        this.allTasksCount = allTasksCount;
    }

    public String getProgress() {
        return progress;
    }

    public void setProgress(String progress) {
        this.progress = progress;
    }

    public Boolean getCompleted() {
        return completed;
    }

    public void setCompleted(Boolean completed) {
        this.completed = completed;
    }

    public Object getPayment() {
        return payment;
    }

    public void setPayment(Object payment) {
        this.payment = payment;
    }

    public Boolean getAborted() {
        return aborted;
    }

    public void setAborted(Boolean aborted) {
        this.aborted = aborted;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public String getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Integer getPolymorphicCtype() {
        return polymorphicCtype;
    }

    public void setPolymorphicCtype(Integer polymorphicCtype) {
        this.polymorphicCtype = polymorphicCtype;
    }
}
