package com.example.myapplication.model.marker;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Marker extends RealmObject {

    @PrimaryKey
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("owner_name")
    @Expose
    private String ownerName;
    @SerializedName("owner_url")
    @Expose
    private String ownerUrl;
    @SerializedName("is_private")
    @Expose
    private Boolean isPrivate;
    @SerializedName("deleted")
    @Expose
    private Boolean deleted;
    @SerializedName("is_recommended")
    @Expose
    private Boolean isRecommended;
    @SerializedName("logo")
    @Expose
    private String logo;
    @SerializedName("thumbnail")
    @Expose
    private String thumbnail;
    @SerializedName("location")
    @Expose
    private Location location;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("lat")
    @Expose
    private Double lat;
    @SerializedName("lon")
    @Expose
    private Double lon;
    @SerializedName("event")
    @Expose
    private String event;
    @SerializedName("comments_count")
    @Expose
    private Integer commentsCount;
    @SerializedName("date_created")
    @Expose
    private String dateCreated;
    @SerializedName("tags")
    @Expose
    private RealmList<String> tags = null;
    @SerializedName("like_count")
    @Expose
    private Integer likeCount;
    @SerializedName("liked")
    @Expose
    private Boolean liked;
    @SerializedName("permissions")
    @Expose
    private Permissions permissions;
    @SerializedName("gallery")
    @Expose
    private RealmList<Gallery> gallery = null;
    @SerializedName("audio")
    @Expose
    private String audio;
    @SerializedName("audio_name")
    @Expose
    private String audioName;
    @SerializedName("rating")
    @Expose
    private String rating;
    @SerializedName("content")
    @Expose
    private String content;
    @SerializedName("followers")
    @Expose
    private RealmList<String> followers = null;
    @SerializedName("group")
    @Expose
    private Group group;
    @SerializedName("publisher")
    @Expose
    private Integer publisher;
    @SerializedName("original")
    @Expose
    private String original;
    @SerializedName("draft")
    @Expose
    private Boolean draft;
    @SerializedName("recommendations")
    @Expose
    private RealmList<String> recommendations = null;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getOwnerUrl() {
        return ownerUrl;
    }

    public void setOwnerUrl(String ownerUrl) {
        this.ownerUrl = ownerUrl;
    }

    public Boolean getIsPrivate() {
        return isPrivate;
    }

    public void setIsPrivate(Boolean isPrivate) {
        this.isPrivate = isPrivate;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Boolean getIsRecommended() {
        return isRecommended;
    }

    public void setIsRecommended(Boolean isRecommended) {
        this.isRecommended = isRecommended;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    public Object getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public Integer getCommentsCount() {
        return commentsCount;
    }

    public void setCommentsCount(Integer commentsCount) {
        this.commentsCount = commentsCount;
    }

    public String getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    public RealmList<String> getTags() {
        return tags;
    }

    public void setTags(RealmList<String> tags) {
        this.tags = tags;
    }

    public Integer getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(Integer likeCount) {
        this.likeCount = likeCount;
    }

    public Boolean getLiked() {
        return liked;
    }

    public void setLiked(Boolean liked) {
        this.liked = liked;
    }

    public Permissions getPermissions() {
        return permissions;
    }

    public void setPermissions(Permissions permissions) {
        this.permissions = permissions;
    }

    public RealmList<Gallery> getGallery() {
        return gallery;
    }

    public void setGallery(RealmList<Gallery> gallery) {
        this.gallery = gallery;
    }

    public Object getAudio() {
        return audio;
    }

    public void setAudio(String audio) {
        this.audio = audio;
    }

    public String getAudioName() {
        return audioName;
    }

    public void setAudioName(String audioName) {
        this.audioName = audioName;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public RealmList<String> getFollowers() {
        return followers;
    }

    public void setFollowers(RealmList<String> followers) {
        this.followers = followers;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public Integer getPublisher() {
        return publisher;
    }

    public void setPublisher(Integer publisher) {
        this.publisher = publisher;
    }

    public String getOriginal() {
        return original;
    }

    public void setOriginal(String original) {
        this.original = original;
    }

    public Boolean getDraft() {
        return draft;
    }

    public void setDraft(Boolean draft) {
        this.draft = draft;
    }

    public List<String> getRecommendations() {
        return recommendations;
    }

    public void setRecommendations(RealmList<String> recommendations) {
        this.recommendations = recommendations;
    }

}