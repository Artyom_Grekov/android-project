package com.example.myapplication.model.marker;

import java.util.HashMap;
import java.util.Map;

//
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LightweightMarker {
    @SerializedName("logo__thumbnail")
    @Expose
    private String logoThumbnail;
    @SerializedName("location__address__city__name")
    @Expose
    private String locationAddressCityName;
    @SerializedName("location__lat")
    @Expose
    private Double locationLat;
    @SerializedName("location__address__house")
    @Expose
    private String locationAddressHouse;
    @SerializedName("location__address__street")
    @Expose
    private String locationAddressStreet;
    @SerializedName("logo__image")
    @Expose
    private String logoImage;
    @SerializedName("location__lon")
    @Expose
    private Double locationLon;
    @SerializedName("location__address__country__name")
    @Expose
    private String locationAddressCountryName;
    @SerializedName("location__address__region__name")
    @Expose
    private String locationAddressRegionName;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;

    public String getLogoImage() {
        return logoImage;
    }

    public void setLogoImage(String logoImage) {
        this.logoImage = logoImage;
    }

    public String getLogoThumbnail() {
        return logoThumbnail;
    }

    public void setLogoThumbnail(String logoThumbnail) {
        this.logoThumbnail = logoThumbnail;
    }

    public String getLocationAddressStreet() {
        return locationAddressStreet;
    }

    public void setLocationAddressStreet(String locationAddressStreet) {
        this.locationAddressStreet = locationAddressStreet;
    }

    public Double getLocationLon() {
        return locationLon;
    }

    public void setLocationLon(Double locationLon) {
        this.locationLon = locationLon;
    }

    public String getLocationAddressHouse() {
        return locationAddressHouse;
    }

    public void setLocationAddressHouse(String locationAddressHouse) {
        this.locationAddressHouse = locationAddressHouse;
    }

    public Double getLocationLat() {
        return locationLat;
    }

    public void setLocationLat(Double locationLat) {
        this.locationLat = locationLat;
    }

    public String getLocationAddressCityName() {
        return locationAddressCityName;
    }

    public void setLocationAddressCityName(String locationAddressCityName) {
        this.locationAddressCityName = locationAddressCityName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocationAddressCountryName() {
        return locationAddressCountryName;
    }

    public void setLocationAddressCountryName(String locationAddressCountryName) {
        this.locationAddressCountryName = locationAddressCountryName;
    }

    public String getLocationAddressRegionName() {
        return locationAddressRegionName;
    }

    public void setLocationAddressRegionName(String locationAddressRegionName) {
        this.locationAddressRegionName = locationAddressRegionName;
    }

}
