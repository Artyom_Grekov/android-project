package com.example.myapplication.model.route;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmList;
import io.realm.RealmObject;

public class Value extends RealmObject {

    @SerializedName("routes_organizer")
    @Expose
    private String routesOrganizer;
    @SerializedName("routes_pdf")
    @Expose
    private String routesPdf;
    @SerializedName("routes_dates")
    @Expose
    private RealmList<String> routesDates = null;//тип объект
    @SerializedName("routes_people_number")
    @Expose
    private Integer routesPeopleNumber;
    @SerializedName("routes_services")
    @Expose
    private RealmList<String> routesServices = null;//тип объект
    @SerializedName("routes_short_program")
    @Expose
    private String routesShortProgram;
    @SerializedName("routes_instructor")
    @Expose
    private RoutesInstructor routesInstructor;
    @SerializedName("routes_email")
    @Expose
    private String routesEmail;
    @SerializedName("routes_phone")
    @Expose
    private String routesPhone;
    @SerializedName("routes_jpg")
    @Expose
    private String routesJpg;

    public String getRoutesOrganizer() {
        return routesOrganizer;
    }

    public void setRoutesOrganizer(String routesOrganizer) {
        this.routesOrganizer = routesOrganizer;
    }

    public String getRoutesPdf() {
        return routesPdf;
    }

    public void setRoutesPdf(String routesPdf) {
        this.routesPdf = routesPdf;
    }

    public RealmList<String> getRoutesDates() {
        return routesDates;
    }

    public void setRoutesDates(RealmList<String> routesDates) {
        this.routesDates = routesDates;
    }

    public Integer getRoutesPeopleNumber() {
        return routesPeopleNumber;
    }

    public void setRoutesPeopleNumber(Integer routesPeopleNumber) {
        this.routesPeopleNumber = routesPeopleNumber;
    }

    public RealmList<String> getRoutesServices() {
        return routesServices;
    }

    public void setRoutesServices(RealmList<String> routesServices) {
        this.routesServices = routesServices;
    }

    public String getRoutesShortProgram() {
        return routesShortProgram;
    }

    public void setRoutesShortProgram(String routesShortProgram) {
        this.routesShortProgram = routesShortProgram;
    }

    public RoutesInstructor getRoutesInstructor() {
        return routesInstructor;
    }

    public void setRoutesInstructor(RoutesInstructor routesInstructor) {
        this.routesInstructor = routesInstructor;
    }

    public String getRoutesEmail() {
        return routesEmail;
    }

    public void setRoutesEmail(String routesEmail) {
        this.routesEmail = routesEmail;
    }

    public String getRoutesPhone() {
        return routesPhone;
    }

    public void setRoutesPhone(String routesPhone) {
        this.routesPhone = routesPhone;
    }

    public String getRoutesJpg() {
        return routesJpg;
    }

    public void setRoutesJpg(String routesJpg) {
        this.routesJpg = routesJpg;
    }

}