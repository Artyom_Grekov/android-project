package com.example.myapplication.model.route;

import com.example.myapplication.model.marker.Marker;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Route  extends RealmObject {
    @PrimaryKey
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("background_image")
    @Expose
    private String backgroundImage;
    @SerializedName("content")
    @Expose
    private Content content;
    @SerializedName("date_created")
    @Expose
    private String dateCreated;
    @SerializedName("gallery")
    @Expose
    private RealmList<String> gallery = null;
    @SerializedName("logo")
    @Expose
    private String logo;
    @SerializedName("liked")
    @Expose
    private Boolean liked;
    @SerializedName("like_count")
    @Expose
    private Integer likeCount;
    @SerializedName("marker_count")
    @Expose
    private Integer markerCount;
    @SerializedName("creator")
    @Expose
    private String creator;
    @SerializedName("permissions")
    @Expose
    private RoutePermissions permissions;
    @SerializedName("purchased")
    @Expose
    private Boolean purchased;
    @SerializedName("purchase_count")
    @Expose
    private Integer purchaseCount;
    @SerializedName("rating")
    @Expose
    private String rating;
    @SerializedName("requires_payment")
    @Expose
    private Boolean requiresPayment;
    @SerializedName("thumbnail")
    @Expose
    private String thumbnail;
    @SerializedName("was_bought_by_users")
    @Expose
    private Boolean wasBoughtByUsers;
    @SerializedName("recommendations")
    @Expose
    private RealmList<Recommendation> recommendations = null;
    @SerializedName("language_code")
    @Expose
    private String languageCode;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("visible")
    @Expose
    private Boolean visible;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("duration")
    @Expose
    private Integer duration;
    @SerializedName("distance")
    @Expose
    private Integer distance;
    @SerializedName("cities")
    @Expose
    private String cities;
    @SerializedName("belongs_to")
    @Expose
    private String belongsTo;
    @SerializedName("date_modified")
    @Expose
    private String dateModified;

    private RealmList<Integer> markersId = new RealmList<Integer>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBackgroundImage() {
        return backgroundImage;
    }

    public void setBackgroundImage(String backgroundImage) {
        this.backgroundImage = backgroundImage;
    }

    public Object getContent() {
        return content;
    }

    public void setContent(Content content) {
        this.content = content;
    }

    public String getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    public List<String> getGallery() {
        return gallery;
    }

    public void setGallery(RealmList<String> gallery) {
        this.gallery = gallery;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public Boolean getLiked() {
        return liked;
    }

    public void setLiked(Boolean liked) {
        this.liked = liked;
    }

    public Integer getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(Integer likeCount) {
        this.likeCount = likeCount;
    }

    public Integer getMarkerCount() {
        return markerCount;
    }

    public void setMarkerCount(Integer markerCount) {
        this.markerCount = markerCount;
    }

    public Object getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public RoutePermissions getPermissions() {
        return permissions;
    }

    public void setPermissions(RoutePermissions permissions) {
        this.permissions = permissions;
    }

    public Boolean getPurchased() {
        return purchased;
    }

    public void setPurchased(Boolean purchased) {
        this.purchased = purchased;
    }

    public Integer getPurchaseCount() {
        return purchaseCount;
    }

    public void setPurchaseCount(Integer purchaseCount) {
        this.purchaseCount = purchaseCount;
    }

    public Object getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public Boolean getRequiresPayment() {
        return requiresPayment;
    }

    public void setRequiresPayment(Boolean requiresPayment) {
        this.requiresPayment = requiresPayment;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public Boolean getWasBoughtByUsers() {
        return wasBoughtByUsers;
    }

    public void setWasBoughtByUsers(Boolean wasBoughtByUsers) {
        this.wasBoughtByUsers = wasBoughtByUsers;
    }

    public RealmList<Recommendation> getRecommendations() {
        return recommendations;
    }

    public void setRecommendations(RealmList<Recommendation> recommendations) {
        this.recommendations = recommendations;
    }

    public String getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getVisible() {
        return visible;
    }

    public void setVisible(Boolean visible) {
        this.visible = visible;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Integer getDistance() {
        return distance;
    }

    public void setDistance(Integer distance) {
        this.distance = distance;
    }

    public String getCities() {
        return cities;
    }

    public void setCities(String cities) {
        this.cities = cities;
    }

    public String getBelongsTo() {
        return belongsTo;
    }

    public void setBelongsTo(String belongsTo) {
        this.belongsTo = belongsTo;
    }

    public String getDateModified() {
        return dateModified;
    }

    public void setDateModified(String dateModified) {
        this.dateModified = dateModified;
    }

    public RealmList<Integer> getMarkersId() {
        return markersId;
    }

    public void setMarkersId(RealmList<Integer> markersId) {
        this.markersId = markersId;
    }
    public void addMarker(Integer markerId) {
        markersId.add(markerId);
    }
}

