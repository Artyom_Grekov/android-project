package com.example.myapplication.model.marker;

import java.util.HashMap;
import java.util.Map;

import io.realm.RealmObject;

public class Group extends RealmObject {

    private Integer id;
    private String name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}