package com.example.myapplication.model.route;

import java.util.ArrayList;
import java.util.List;

public class Routes {
    private Integer count;
    private Object next;
    private Object previous;
    private List<Route> results = new ArrayList<Route>();

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Object getNext() {
        return next;
    }

    public void setNext(Object next) {
        this.next = next;
    }

    public Object getPrevious() {
        return previous;
    }

    public void setPrevious(Object previous) {
        this.previous = previous;
    }

    public List<Route> getResults() {
        return results;
    }

    public void setResults(List<Route> results) {
        this.results = results;
    }
}
