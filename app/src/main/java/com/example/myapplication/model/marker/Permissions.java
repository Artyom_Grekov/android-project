package com.example.myapplication.model.marker;

import java.util.HashMap;
import java.util.Map;

import io.realm.RealmObject;

public class Permissions extends RealmObject {

    private Boolean share;
    private Boolean modify;
    private Boolean editPermissions;
    private Boolean delete;

    public Boolean getShare() {
        return share;
    }

    public void setShare(Boolean share) {
        this.share = share;
    }

    public Boolean getModify() {
        return modify;
    }

    public void setModify(Boolean modify) {
        this.modify = modify;
    }

    public Boolean getEditPermissions() {
        return editPermissions;
    }

    public void setEditPermissions(Boolean editPermissions) {
        this.editPermissions = editPermissions;
    }

    public Boolean getDelete() {
        return delete;
    }

    public void setDelete(Boolean delete) {
        this.delete = delete;
    }

}