package com.example.myapplication.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;
import com.example.myapplication.retrofit.NetworkService;
import com.example.myapplication.model.route.Route;
import com.squareup.picasso.Picasso;

import java.util.List;

public class RoutesAdapter extends RecyclerView.Adapter<RoutesAdapter.ViewHolder>{
    private List<Route> routes;
    private LayoutInflater inflater;
    private int index;
    private static RoutesAdapter.OnClickListener listener;

    public interface OnClickListener {
        void onClick(View view, int position);
    }
    public RoutesAdapter(Context context, List<Route> routes){
        this.inflater = LayoutInflater.from(context);
        this.routes = routes;
    }

    @NonNull
    @Override
    public RoutesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.routes_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RoutesAdapter.ViewHolder holder, int position) {
        String name = routes.get(position).getName();
        holder.nameRoute.setText(name);

        if(routes.get(position).getPurchased()) {
            holder.purchased.setText(holder.purchased.getText() + " да");
        } else {
            holder.purchased.setText(holder.purchased.getText() + " нет");
        }

        String url = NetworkService.getInstance().getBaseUrl() + routes.get(position).getLogo();

        Picasso.get().load(url).placeholder(R.drawable.man).error(R.drawable.cross).into(holder.routeImage);
    }

    public void setListener(OnClickListener listener) {
        RoutesAdapter.listener = listener;
    }



    @Override
    public int getItemCount() {
        return routes.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView  nameRoute;
        public TextView  purchased;
        public ImageView routeImage;
        public LinearLayout layout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            this.nameRoute  = (TextView)     itemView.findViewById(R.id.routeName);
            this.purchased  = (TextView)     itemView.findViewById(R.id.purchased);
            this.routeImage = (ImageView)    itemView.findViewById(R.id.routeImage);
            this.layout     = (LinearLayout) itemView.findViewById(R.id.routeLinear);

            layout.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            listener.onClick(v, position);
        }
    }
}
