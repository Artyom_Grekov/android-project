package com.example.myapplication.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;
import com.example.myapplication.model.marker.Marker;
import com.example.myapplication.retrofit.NetworkService;
import com.squareup.picasso.Picasso;

import java.util.List;

public class MarkersAdapter extends RecyclerView.Adapter<MarkersAdapter.ViewHolder>{
    private List<Marker> markers;
    private LayoutInflater inflater;
    private int index;
    private static MarkersAdapter.OnClickListener listener;

    public interface OnClickListener {
        void onClick(View view, int position);
    }
    public MarkersAdapter(Context context, List<Marker> markers){
        this.inflater = LayoutInflater.from(context);
        this.markers = markers;
    }

    @NonNull
    @Override
    public MarkersAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.marker_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MarkersAdapter.ViewHolder holder, int position) {
        String name = markers.get(position).getName();
        holder.nameMarker.setText(name);


        String url = NetworkService.getInstance().getBaseUrl() + markers.get(position).getLogo();

        Picasso.get().load(url).placeholder(R.drawable.man).error(R.drawable.cross).into(holder.markerImage);
    }

    public void setListener(OnClickListener listener) {
        MarkersAdapter.listener = listener;
    }

    @Override
    public int getItemCount() {
        return markers.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView nameMarker;
        public ImageView markerImage;
        public LinearLayout layout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            this.nameMarker  = (TextView)     itemView.findViewById(R.id.markerName);
            this.markerImage = (ImageView)    itemView.findViewById(R.id.markerImage);
            this.layout      = (LinearLayout) itemView.findViewById(R.id.markerLinear);

            layout.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            listener.onClick(v, position);
        }
    }
}
