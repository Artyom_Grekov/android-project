package com.example.myapplication.repository;

import com.example.myapplication.model.route.Route;

import java.util.List;

public interface IRouteDao {
    void addRoute(Route route);
    void removeRoute(int id);
    List getAllRoutes();
    Route getRoute(int id);
    void deleteAll();
    List<Route> sortByName();
    List<Route> sortById();
    List<Route> sortByCountMarkers();
}
