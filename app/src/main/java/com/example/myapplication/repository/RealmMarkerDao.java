package com.example.myapplication.repository;

import com.example.myapplication.model.marker.Marker;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

public class RealmMarkerDao implements IMarkerDao  {
    @Override
    public void addMarker(Marker marker) {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();

        Marker markerRealm = realm.copyToRealm(marker);

        realm.commitTransaction();
        realm.close();
    }

    @Override
    public void removeMarker(int id) {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();

        Marker marker = realm.where(Marker.class).equalTo("id", id).findFirst();

        marker.deleteFromRealm();

        realm.commitTransaction();
        realm.close();
    }

    @Override
    public List getAllMarker() {
        Realm realm = Realm.getDefaultInstance();

        RealmResults<Marker> marker = realm.where(Marker.class).findAll();

        List returnMarker = realm.copyFromRealm(marker);

        realm.close();

        return returnMarker;
    }

    @Override
    public Marker getMarker(int id) {
        Realm realm = Realm.getDefaultInstance();

        Marker marker = realm.where(Marker.class).equalTo("id", id).findFirst();

        Marker returnMarker = realm.copyFromRealm(marker);
        realm.close();
        return returnMarker;
    }
}
