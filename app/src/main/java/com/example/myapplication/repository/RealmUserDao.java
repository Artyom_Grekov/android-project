package com.example.myapplication.repository;

import com.example.myapplication.model.authorization.User;

import io.realm.Realm;
import io.realm.RealmResults;

public class RealmUserDao implements IUserDao {
    @Override
    public void addUser(User user) {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();

        User userRealm = realm.copyToRealm(user);

        realm.commitTransaction();
        realm.close();
    }

    @Override
    public void removeUser() {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();

        RealmResults<User> users = realm.where(User.class).findAll();

        users.deleteAllFromRealm();

        realm.commitTransaction();
        realm.close();
    }

    @Override
    public User getUser() {
        Realm realm = Realm.getDefaultInstance();

        RealmResults<User> users = realm.where(User.class).findAll();

        User returnUser;
        if(users.size() > 0) {
            returnUser = realm.copyFromRealm(users.get(0));

            realm.close();
            return returnUser;
        }

        realm.close();
        return null;
    }

    @Override
    public boolean haveUser() {
        Realm realm = Realm.getDefaultInstance();

        RealmResults<User> users = realm.where(User.class).findAll();

        boolean haveUser = (users.size() > 0);
        realm.close();

        return haveUser;
    }
}
