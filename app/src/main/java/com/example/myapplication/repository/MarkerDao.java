package com.example.myapplication.repository;

import com.example.myapplication.model.marker.Marker;

import java.util.ArrayList;
import java.util.List;

public class MarkerDao implements IMarkerDao {
    private List<Marker> markers;

    public MarkerDao() {
        markers = new ArrayList<Marker>();
    }
    @Override
    public void addMarker(Marker marker) {
        markers.add(marker);
    }

    @Override
    public void removeMarker(int id) {
        markers.remove(id);
    }

    @Override
    public List getAllMarker() {
        return markers;
    }

    @Override
    public Marker getMarker(int id) {
        return markers.get(id);
    }
}
