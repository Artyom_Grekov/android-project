package com.example.myapplication.repository;

import com.example.myapplication.model.marker.Marker;

import java.util.List;

public interface IMarkerDao {
    void addMarker(Marker marker);
    void removeMarker(int id);
    List getAllMarker();
    Marker getMarker(int id);
}
