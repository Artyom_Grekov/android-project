package com.example.myapplication.repository;

import com.example.myapplication.model.route.Route;

import java.util.ArrayList;
import java.util.List;

public class RouteDao implements IRouteDao {
    private List<Route> routes;

    public RouteDao() {
        routes = new ArrayList<Route>();
    }

    @Override
    public void addRoute(Route route) {
        routes.add(route);
    }

    @Override
    public void removeRoute(int id) {
        routes.remove(id);
    }

    @Override
    public List getAllRoutes() {
        return routes;
    }

    @Override
    public Route getRoute(int id) {

        for(int i = 0; i < routes.size(); i++)
            if (routes.get(i).getId() == id) {
                return routes.get(i);
            }
        return routes.get(1);//исправить
    }

    @Override
    public void deleteAll() {

    }

    @Override
    public List<Route> sortByName() {
        return null;
    }

    @Override
    public List<Route> sortById() {
        return null;
    }

    @Override
    public List<Route> sortByCountMarkers() {
        return null;
    }
}
