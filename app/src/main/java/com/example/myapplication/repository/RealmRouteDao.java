package com.example.myapplication.repository;

import com.example.myapplication.model.route.Route;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

public class RealmRouteDao implements IRouteDao {
    @Override
    public void addRoute(Route route) {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();

        Route routeRealm = realm.copyToRealm(route);

        realm.commitTransaction();
        realm.close();
    }

    @Override
    public void removeRoute(int id) {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();

        Route routes = realm.where(Route.class).equalTo("id", id).findFirst();

        routes.deleteFromRealm();

        realm.commitTransaction();
        realm.close();
    }

    @Override
    public List getAllRoutes() {
        Realm realm = Realm.getDefaultInstance();

        RealmResults<Route> routes = realm.where(Route.class).findAll();

        List returnRoutes = realm.copyFromRealm(routes);

        realm.close();

        return returnRoutes;
    }

    @Override
    public Route getRoute(int id) {
        Realm realm = Realm.getDefaultInstance();

        Route routes = realm.where(Route.class).equalTo("id", id).findFirst();

        /*if(routes.size() > 0) {
            Route returnRoute = realm.copyFromRealm(routes.get(0));
            realm.close();
            return returnRoute;
        }*/

        Route returnRoute = realm.copyFromRealm(routes);
        realm.close();
        return returnRoute;
        /*
        realm.close();
        return null;*/
    }

    @Override
    public void deleteAll() {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();

        RealmResults<Route> routes = realm.where(Route.class).findAll();

        routes.deleteAllFromRealm();

        realm.commitTransaction();
        realm.close();
    }

    @Override
    public List<Route> sortByName() {
        Realm realm = Realm.getDefaultInstance();

        RealmResults<Route> routes = realm.where(Route.class).sort("name").findAll();

        List returnRoutes = realm.copyFromRealm(routes);

        realm.close();

        return returnRoutes;
    }

    @Override
    public List<Route> sortById() {
        Realm realm = Realm.getDefaultInstance();

        RealmResults<Route> routes = realm.where(Route.class).sort("id").findAll();

        List returnRoutes = realm.copyFromRealm(routes);

        realm.close();

        return returnRoutes;
    }

    @Override
    public List<Route> sortByCountMarkers() {
        Realm realm = Realm.getDefaultInstance();

        RealmResults<Route> routes = realm.where(Route.class).sort("markerCount").findAll();

        List returnRoutes = realm.copyFromRealm(routes);

        realm.close();

        return returnRoutes;
    }
}
