package com.example.myapplication.repository;

import com.example.myapplication.model.authorization.User;

import java.util.List;

public interface IUserDao {
    void addUser(User user);
    void removeUser();
    User getUser();
    boolean haveUser();
}
