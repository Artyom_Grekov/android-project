package com.example.myapplication.repository;

import com.example.myapplication.model.authorization.User;

public class UserDao implements IUserDao {
    private User user;

    public UserDao(User user) {
        this.user = user;
    }
    public UserDao() {

    }

    @Override
    public void addUser(User user) {
        this.user = user;
    }

    @Override
    public void removeUser() {
        user = null;
    }

    @Override
    public User getUser() {
        return user;
    }

    @Override
    public boolean haveUser() {
        return (user != null);
    }
}
