package com.example.myapplication.retrofit.interfaces;

import com.example.myapplication.model.marker.LightweightMarker;
import com.example.myapplication.model.marker.LightweightMarkers;
import com.example.myapplication.model.marker.Marker;
import com.example.myapplication.model.route.Route;
import com.example.myapplication.model.route.Routes;
import com.example.myapplication.model.authorization.RequestAuthorization;
import com.example.myapplication.model.authorization.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface YouGidApi {
    @Headers("Content-Type: application/json;charset=UTF-8")
    @POST("api/v2/token-auth/")
    Call<User> authorization(@Body RequestAuthorization request);

    @Headers("Content-Type: application/json;charset=UTF-8")
    @GET("api/v2/group/{group_id}/route/")
    Call<Routes> getAllRoutes(@Header("Authorization") String token, @Path("group_id") String groupId);

    @GET("api/v2/route/{route_id}")
    Call<Route> getRoute(@Path("route_id") int routeId);

    @Headers("Content-Type: application/json;charset=UTF-8")
    @GET("api/v2/marker/{marker_id}/") // получение полной метки
    Call<Marker> getMarker(@Path("marker_id") int marker_id);
         //api/v2/route/10        /marker_lightweight/
    /*@GET("api/v2/route/{route_id}/marker_lightweight/") //получение легких меток
    Call<LightweightMarker> getLightweightMarkers(@Path("route_id") int routeId);*/

    @Headers("Content-Type: application/json;charset=UTF-8")
    @GET("api/v2/route/{route_id}/marker_lightweight/") //получение легких меток
    Call<List<LightweightMarker>> getLightweightMarkersList(@Path("route_id") int routeId);

    @Headers("Content-Type: application/json;charset=UTF-8")
    @GET("api/v2/route/10/marker_lightweight/") //получение легких меток
    Call<LightweightMarker> getLightweightMarkers(@Path("route_id") int routeId);

}
