package com.example.myapplication.Thread;

import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.myapplication.Singleton.Singleton;
import com.example.myapplication.model.marker.LightweightMarker;
import com.example.myapplication.model.marker.Marker;
import com.example.myapplication.model.route.Route;
import com.example.myapplication.retrofit.NetworkService;

import java.io.IOException;
import java.util.List;

import retrofit2.Response;

public class LoadMarkersThread extends Thread{
    private Route route;
    private List<LightweightMarker> lightMarkers;
    private AppCompatActivity context;

    public LoadMarkersThread(Route route, List<LightweightMarker> lightMarkers, AppCompatActivity context) {
        this.route = route;
        this.lightMarkers = lightMarkers;
        this.context = context;
    }
    public LoadMarkersThread(Route route, List<LightweightMarker> lightMarkers) {
        this.route = route;
        this.lightMarkers = lightMarkers;
    }
    @Override
    public void run() {
        for(LightweightMarker lightMarker: lightMarkers) {
            try {
                Response<Marker> response = NetworkService.getInstance().getYouGidApi().getMarker(lightMarker.getId()).execute();

                Marker marker = response.body();
                Singleton.getMarkerService().addMarker(marker);

                route.addMarker(marker.getId());
            } catch (IOException e) {
                e.printStackTrace();
            }


        }

        Singleton.getRouteService().addRoute(route);
        /*String text = "Успешно";
        int duration = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(context, text, duration);
        toast.show();*/
    }
}
