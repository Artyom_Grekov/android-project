package com.example.myapplication.Singleton;

import com.example.myapplication.repository.IMarkerDao;
import com.example.myapplication.service.IMarkerService;
import com.example.myapplication.service.IUserService;
import com.example.myapplication.service.RouteService;
import com.example.myapplication.service.WebService;

public class Singleton {
    private static RouteService  routeService;
    private static WebService    webService;
    private static IUserService  userService;
    private static IMarkerService markerService;

    public static void setMarkerService(IMarkerService markerService){
        Singleton.markerService = markerService;
    }

    public static IMarkerService getMarkerService() {
        return markerService;
    }

    public static void setRouteService(RouteService routeService) {
        Singleton.routeService = routeService;
    }

    public static RouteService getRouteService(){
        return routeService;
    }

    public static WebService getWebService() {
        return webService;
    }

    public static void setWebService(WebService webService) {
        Singleton.webService = webService;
    }

    public static IUserService getUserService() {
        return userService;
    }

    public static void setUserService(IUserService userService) {
        Singleton.userService = userService;
    }
}
