package com.example.myapplication.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.myapplication.R;
import com.example.myapplication.Singleton.Singleton;
import com.example.myapplication.retrofit.NetworkService;
import com.squareup.picasso.Picasso;

public class UserActivity extends AppCompatActivity {
    private ImageView userImage;
    private TextView  name;
    private TextView  mail;
    private TextView  showName;
    private TextView  showMail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);

        name      = (TextView)  findViewById(R.id.userNameTextView);
        mail      = (TextView)  findViewById(R.id.userMailTextView);
        showName  = (TextView)  findViewById(R.id.showUserName);
        showMail  = (TextView)  findViewById(R.id.showUserMail);
        userImage = (ImageView) findViewById(R.id.userImageView);

        showUserInfo();
    }

    private void showUserInfo(){
        showName.setText(Singleton.getUserService().getUser().getFull_name());
        showMail.setText(Singleton.getUserService().getUser().getEmail());

        String url = NetworkService.getInstance().getBaseUrl() + Singleton.getUserService().getUser().getAvatar();

        Picasso.get().load(url).placeholder(R.drawable.man).error(R.drawable.cross).into(userImage);
    }

    public void onClickLogoutBtn(View view){
        Singleton.getUserService().removeUser();

        Intent intent = new Intent(this, AuthorizationActivity.class);
        startActivity(intent);

        this.finish();
    }
}
