package com.example.myapplication.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.myapplication.R;
import com.example.myapplication.Singleton.Singleton;
import com.example.myapplication.model.route.Route;

public class ShowRouteActivity extends AppCompatActivity {
    private TextView nameRoute;
    private TextView routeDescription;
    private TextView countMarkers;

    private Route route;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_route);

        nameRoute        = findViewById(R.id.routeName);
        routeDescription = findViewById(R.id.descriptionRoute);
        countMarkers     = findViewById(R.id.countMarkers);

        Bundle argument = getIntent().getExtras();
        int id = argument.getInt("id");

        route = Singleton.getRouteService().getRoute(id);

        nameRoute.setText(route.getName());
        routeDescription.setText(route.getDescription());

        String count = Integer.toString(route.getMarkerCount());
        countMarkers.setText(count);

    }

    public void onClickShowAllMarkersBttn(View view) {
        Intent intent = new Intent(this, ShowMarkersActivity.class);
        intent.putExtra("id", route.getId());
        startActivity(intent);

    }
}
