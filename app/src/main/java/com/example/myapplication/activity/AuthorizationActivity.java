package com.example.myapplication.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import com.example.myapplication.R;
import com.example.myapplication.Singleton.Singleton;

public class AuthorizationActivity extends AppCompatActivity {
    private Button button;
    private EditText name;
    private EditText password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_authorization);

        button     = (Button)   findViewById(R.id.button2);
        name       = (EditText) findViewById(R.id.name);
        password   = (EditText) findViewById(R.id.password);

        name.setText("artyom.grekov@gmail.com");
        password.setText("JasonBrodyKaKEvklid536");
    }

    public void onClickAuthButton(View view){
        String nameUser     = name.getText().toString();
        String passwordUser = password.getText().toString();


        Singleton.getWebService().authorization(nameUser, passwordUser, this);
        //this.finish();
    }
}
