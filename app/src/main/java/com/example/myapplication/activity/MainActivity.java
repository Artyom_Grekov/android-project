package com.example.myapplication.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.myapplication.R;
import com.example.myapplication.Singleton.Singleton;
import com.example.myapplication.model.route.Route;
import com.example.myapplication.repository.IMarkerDao;
import com.example.myapplication.repository.RealmMarkerDao;
import com.example.myapplication.repository.RealmRouteDao;
import com.example.myapplication.repository.RealmUserDao;
import com.example.myapplication.repository.UserDao;
import com.example.myapplication.retrofit.NetworkService;
import com.example.myapplication.service.MarkerService;
import com.example.myapplication.service.RouteService;
import com.example.myapplication.service.UserService;
import com.example.myapplication.service.WebService;
import com.squareup.picasso.Picasso;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class MainActivity extends AppCompatActivity {
    private ImageView userImage;
    private RouteService routeService;
    private WebService webService;
    private UserService userService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /*RealmConfiguration config = new RealmConfiguration.Builder()
                .deleteRealmIfMigrationNeeded()
                .build();*/

       

        Realm.init(this); //Инициализация Realm
        //Realm.deleteRealm(Realm.getDefaultConfiguration());


        /*Realm realm = Realm.getDefaultInstance();
        try {
            realm.close();
            Realm.deleteRealm(realm.getConfiguration());
            //Realm file has been deleted.
        } catch (Exception ex){
            ex.printStackTrace();
            //No Realm file to remove.
        }*/

        IMarkerDao markerDao = new RealmMarkerDao();

        userImage = (ImageView) findViewById(R.id.userImage);

        Singleton.setMarkerService(new MarkerService(markerDao));

        routeService = new RouteService(new RealmRouteDao(), markerDao);
        Singleton.setRouteService(routeService);
        webService = new WebService();
        Singleton.setWebService(webService);
        Singleton.setUserService(new UserService(new RealmUserDao()));

        checkAuthorization();
    }

    private void checkAuthorization() {
        if(!Singleton.getUserService().haveUser()) {
            Intent intent = new Intent(this, AuthorizationActivity.class);
            startActivity(intent);
        }
    }

    public void onClickAuthBtn(View view){
        Intent intent = new Intent(this, AuthorizationActivity.class);
        startActivity(intent);
    }

    public void onClickShowRoutesBtn(View view){
        Intent intent = new Intent(this, ShowRoutesActivity.class);
        startActivity(intent);
    }

    public void onClickLoadBtn(View view){
        webService.loadRoutes(this);//исправить
        //webService.loadLightMarkersList(10);

        /*for(Route route: routeService.getAllRoutes()) {
            webService.loadLightMarkersList(route.getId());
        }*/
    }

    @Override
    protected void onResume() {
        super.onResume();

        try {
            String url = NetworkService.getInstance().getBaseUrl() + Singleton.getUserService().getUser().getAvatar();

            Picasso.get().load(url).placeholder(R.drawable.man).error(R.drawable.cross).into(userImage);
        }catch (Exception e) {
            Picasso.get().load(R.drawable.cross).into(userImage);
        }

    }

    public void onClickUserImage(View view) {
        if(Singleton.getUserService().getUser() != null) {
            Intent intent = new Intent(this, UserActivity.class);
            startActivity(intent);
        }
    }

    public void onClickDeleteAllBtn(View view){
        Singleton.getRouteService().deleteAll();
    }

}
