package com.example.myapplication.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.myapplication.R;
import com.example.myapplication.Singleton.Singleton;
import com.example.myapplication.model.marker.Marker;
import com.example.myapplication.retrofit.NetworkService;
import com.squareup.picasso.Picasso;

public class ShowMarkerActivity extends AppCompatActivity {
    private Marker marker;

    private TextView  markerName;
    private TextView  markerDescription;
    private TextView  markerAdress;
    private ImageView markerImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_marker);

        init();
        printText();
        loadImage();
    }

    private void init() {
        Bundle argument = getIntent().getExtras();
        int id = argument.getInt("id");

        marker = Singleton.getMarkerService().getMarker(id);

        markerName        = findViewById(R.id.markerName);
        markerAdress      = findViewById(R.id.markerAdress);
        markerDescription = findViewById(R.id.markerDescription);
        markerImage       = findViewById(R.id.showMarkerImage);
    }

    private void printText() {
        markerName.setText(marker.getName());
        markerAdress.setText(marker.getAddress());
        markerDescription.setText(marker.getDescription());
    }

    private void loadImage() {
        String url =  NetworkService.getInstance().getBaseUrl() + marker.getLogo();
        Singleton.getWebService().loadImage(markerImage, url);

        //Picasso.get().load(url).placeholder(R.drawable.man).error(R.drawable.cross).into(markerImage);
    }
}
