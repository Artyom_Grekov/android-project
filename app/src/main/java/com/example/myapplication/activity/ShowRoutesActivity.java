package com.example.myapplication.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;
import com.example.myapplication.Singleton.Singleton;
import com.example.myapplication.adapter.RoutesAdapter;
import com.example.myapplication.model.route.Route;
import com.example.myapplication.service.RouteService;

import java.util.List;

public class ShowRoutesActivity extends AppCompatActivity implements RoutesAdapter.OnClickListener{
    private RecyclerView recyclerView;
    private RoutesAdapter adapter;
    private Spinner spinner;
    private int numberType;
    String[] types = {"Названию", "Id", "Количеству меток"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_routes);

        createRecyclerView(1);

        //createSpinner();

    }
    private void createSpinner() {
        spinner = (Spinner) findViewById(R.id.spinnerRoute);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, types);
        // Определяем разметку для использования при выборе элемента
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Применяем адаптер к элементу spinner
        spinner.setAdapter(adapter);

        AdapterView.OnItemSelectedListener itemSelectedListener = new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                createRecyclerView(position);

                /*if(position == 0) {
                    List<Route> routes = Singleton.getRouteService().getAllRoutes();

                    adapter = new RoutesAdapter(this, routes);
                    adapter.setListener(this);
                    recyclerView.setAdapter(adapter);
                }*/
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        };
        spinner.setOnItemSelectedListener(itemSelectedListener);
    }

    private  void createRecyclerView(int position){
        this.numberType = position;

        List<Route> routes;

        switch (position) {
            case 0:
                routes = Singleton.getRouteService().sortByName();
                break;
            case 1:
                routes = Singleton.getRouteService().sortById();
                break;
            case 2:
                routes = Singleton.getRouteService().sortByCountMarkers();
                break;
            default:
                routes = Singleton.getRouteService().sortByCountMarkers();
                break;
        }


        recyclerView = (RecyclerView) findViewById(R.id.routesRecycler);


        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        /*RouteService routeService = Singleton.getRouteService();
        List<Route> routes = routeService.getAllRoutes();*/

        adapter = new RoutesAdapter(this, routes);
        adapter.setListener(this);
        recyclerView.setAdapter(adapter);
    }

    private Intent getNewIntent(int position){
        Intent intent = new Intent(this, ShowRouteActivity.class);

        int id;
        switch (numberType) {
            case 0:
                id = Singleton.getRouteService().sortByName().get(position).getId();
                break;
            case 1:
                id = Singleton.getRouteService().sortById().get(position).getId();
                break;
            case 2:
                id = Singleton.getRouteService().sortByCountMarkers().get(position).getId();
                break;
            default:
                id = Singleton.getRouteService().sortByCountMarkers().get(position).getId();
                break;
        }

        //int id = Singleton.getRouteService().getAllRoutes().get(position).getId();
        intent.putExtra("id", id);
        return intent;
    }



    @Override
    public void onClick(View view, int position) {
        Route route;



        Intent intent = getNewIntent(position);
        startActivity(intent);
    }
}
