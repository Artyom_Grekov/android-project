package com.example.myapplication.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.myapplication.R;
import com.example.myapplication.Singleton.Singleton;
import com.example.myapplication.adapter.MarkersAdapter;
import com.example.myapplication.adapter.RoutesAdapter;
import com.example.myapplication.model.marker.Marker;
import com.example.myapplication.model.route.Route;

import java.util.List;

public class ShowMarkersActivity extends AppCompatActivity implements MarkersAdapter.OnClickListener{
    private RecyclerView recyclerView;
    private MarkersAdapter adapter;
    private Route route;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_markers);

        Bundle argument = getIntent().getExtras();
        int id = argument.getInt("id");

        route = Singleton.getRouteService().getRoute(id);

        createRecyclerView();
    }

    @Override
    public void onClick(View view, int position) {
        Intent intent = getNewIntent(position);
        startActivity(intent);

    }
    private Intent getNewIntent(int position){
        Intent intent = new Intent(this, ShowMarkerActivity.class);

        int id = Singleton.getRouteService().getAllMarkers(route.getId()).get(position).getId();

        intent.putExtra("id", id);
        return intent;
    }

    private  void createRecyclerView(){


        List<Marker> markers = Singleton.getRouteService().getAllMarkers(route.getId());

        recyclerView = (RecyclerView) findViewById(R.id.markersRecycler);


        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        /*RouteService routeService = Singleton.getRouteService();
        List<Route> routes = routeService.getAllRoutes();*/

        adapter = new MarkersAdapter(this, markers);
        adapter.setListener(this);
        recyclerView.setAdapter(adapter);
    }
}
